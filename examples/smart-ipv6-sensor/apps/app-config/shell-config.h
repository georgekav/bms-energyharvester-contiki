/**
 * \file
 *         Header file for Contik shell command config
 * \author
 *         Darko Petrovic
 */

#ifndef SHELL_CONFIG_H_
#define SHELL_CONFIG_H_

#include "shell.h"

void shell_config_init(void);

#endif /* SHELL_CONFIG_H_ */
